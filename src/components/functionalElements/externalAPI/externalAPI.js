import React from 'react';
import axios from 'axios'; //npm install axios

export default class ExternalAPI extends React.Component {

//informations is the json found for that url

constructor(props) { //passing props
        super(props);
        this.state = {
            informations: "no info found",
	    myurl: this.props.myurl,
        };
}

getInitialState() {
    return {information: undefined,}
}

componentDidMount() {//get json as a string
	const url=this.state.myurl;
    	axios.get(url)
    	.then(function (response) {  
		var informations = JSON.stringify(response);
        	this.setState({informations: informations});
    	}.bind(this))
}

    render(){
        return(
		<div>
		<p>Url provided: {this.state.myurl} </p>
  		<p>Your json informations:</p>
		<div>{this.state.informations}</div>
		</div>
            );
    }
}
