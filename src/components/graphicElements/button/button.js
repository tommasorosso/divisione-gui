import React from 'react';
import './button.css';

export default class Button extends React.Component {
    render(){
        return(
            <button type="button" onClick={this.props.function.bind(this)}>{this.props.text}</button>
            );
    }
}