import React from 'react';
import { shallow } from 'enzyme';


import TextView from '../../../src/components/graphicElements/TextView/TextView';

describe('TextView', () => {
    it('should contain a p tag', () => {
        const component = shallow(<TextView text={'This is a textView'} />);
        expect(component.find('p').length).toEqual(1);
    });

    it('should have text', () => {
        const component = shallow(<TextView text={'This is a textView'} />);
        expect(component.text()).toEqual('This is a textView');
    });
});
